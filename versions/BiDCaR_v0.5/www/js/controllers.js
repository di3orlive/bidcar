var appCtrl = angular.module('starter.controllers', []);

appCtrl.factory('driversData', function(){
  return [
    { id: 1, name: 'J Stratham', driver_id: 'ID00549', driver_rating: '4', time_in_path: '5', service_fee: '08.80'},
    { id: 2, name: 'T Driver', driver_id: 'ID00549', driver_rating: '5', time_in_path: '4', service_fee: '09.50'},
    { id: 3, name: 'A Smith', driver_id: 'ID00549', driver_rating: '3', time_in_path: '9', service_fee: '06.80'},
    { id: 4, name: 'J Smith', driver_id: 'ID00549', driver_rating: '5', time_in_path: '3', service_fee: '18.80'},
    { id: 5, name: 'S Jones', driver_id: 'ID00549', driver_rating: '2', time_in_path: '10', service_fee: '28.80'},
    { id: 6, name: 'V Putin', driver_id: 'ID00549', driver_rating: '4', time_in_path: '25', service_fee: '00.00'},
    { id: 7, name: 'J Stratham', driver_id: 'ID00549', driver_rating: '4', time_in_path: '5', service_fee: '08.80'},
    { id: 8, name: 'T Driver', driver_id: 'ID00549', driver_rating: '5', time_in_path: '4', service_fee: '09.50'},
    { id: 9, name: 'A Smith', driver_id: 'ID00549', driver_rating: '3', time_in_path: '9', service_fee: '06.80'},
    { id: 10, name: 'J Smith', driver_id: 'ID00549', driver_rating: '5', time_in_path: '3', service_fee: '18.80'},
    { id: 11, name: 'S Jones', driver_id: 'ID00549', driver_rating: '2', time_in_path: '10', service_fee: '28.80'},
    { id: 12, name: 'V Putin', driver_id: 'ID00549', driver_rating: '4', time_in_path: '25', service_fee: '00.00'}
  ];
});

appCtrl.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
});

appCtrl.controller('DriversCtrl', function($scope, driversData) {
  $scope.drivers = driversData;
});

appCtrl.controller('DriverCtrl', ['$scope', '$stateParams', 'driversData', function ($scope, $stateParams, driversData) {
  $scope.driver = driversData[$stateParams.id -1];
}]);

appCtrl.controller('BidsBeingsCtrl', function($scope, $stateParams) {
});

appCtrl.controller('rateDriverCtrl', function($scope) {
  $scope.tipValue = 0;

  $scope.decraceTip = function(){
    if($scope.tipValue <= 0){
      $scope.tipValue = 0;
      return;
    }
    $scope.tipValue -= 1;
  };
  $scope.incraceTip = function(){
    $scope.tipValue += 1;
  };
});

appCtrl.controller('driverPageCtrl', function($scope) {
  $scope.toggleStatus = false;
});

appCtrl.controller('rideNowCtrl', function($scope) {
  $scope.riders = [
    { id: 1, journey: 'n3-sw11', time: '50', driver_rating: '4', miles: '3.7', cost: '35.50'},
    { id: 2, journey: 'e17-e3', time: '24', driver_rating: '5', miles: '2.5', cost: '23.80'},
    { id: 3, journey: 'se5-e6', time: '14', driver_rating: '3', miles: '2.6', cost: '15.30'},
    { id: 4, journey: 'n1-w4', time: '18', driver_rating: '5', miles: '2.9', cost: '19.20'},
    { id: 5, journey: 'n6-sw10', time: '45', driver_rating: '2', miles: '3.1', cost: '28.20'}
  ];
});

appCtrl.controller('MapCtrl', function($scope,$rootScope) {
  //$scope.ngLoaded = false;
  //
  //$scope.pageLoad = function(){
  //  $scope.ngLoaded = true;
  //  initialize();
  //  console.log('xyu');
  //};



  function initialize() {
    var myLatlng = new google.maps.LatLng(51.5055731,-0.1378339);

    var mapOptions = {
      center: myLatlng,
      zoom: 13,
      disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      icon: './img/marker.png',
      title: 'Uluru (Ayers Rock)'
    });

    $scope.map = map;






    var miles = 3;
    var lineSymbol = {
      path: 'M 0,-0.05 0,0.05',
      strokeColor: '#FF0000',
      strokeOpacity: 1,
      strokeDasharray: 5.5,
      scale: 5
    };

    new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.5,
      strokeWeight: 1,
      fillColor: '#FF0000',
      fillOpacity: 0.1,
      map: map,
      center: myLatlng,
      radius: miles * 1609.344
    });

    var line = new google.maps.Polyline({
      path: [myLatlng, {lat: 52.5055731, lng: -0.2378339}],
      strokeOpacity: 0,
      icons: [{
        icon: lineSymbol,
        offset: '0',
        repeat: '7px'
      }],
      map: map
    });

    map.addListener('click', function(e) {
      new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.5,
        strokeWeight: 1,
        fillColor: '#FF0000',
        fillOpacity: 0.1,
        map: map,
        center: myLatlng,
        radius: miles * 1609.344 * 2
      });
    });
  }
  google.maps.event.addDomListener(window, 'load', initialize);

});

appCtrl.controller('bitBetCtrl', function($scope) {
  $scope.bitBet = 0;

  $scope.decraceBit = function(){
    if($scope.bitBet <= 0){
      $scope.bitBet = 0;
      return;
    }
    $scope.bitBet -= 1;
  };
  $scope.incraceBit = function(){
    $scope.bitBet += 1;
  };
  $scope.resetBit = function(){
    $scope.bitBet = 0;
  };
});

//======================================================================================================================

appCtrl.directive('expiredJob', function($timeout){
  return {
    restrict: "A",
    scope: {
      expiredJob: "@"
    },
    link: function (scope, element) {
      if(scope.expiredJob == 0){
        element.removeClass('active');
      }else{
        var quarter = +scope.expiredJob / 4;

        element.children('.top-right, .bottom-right, .bottom-left, .top-left').css({transition: "all " + quarter + "ms linear"});


        element.children('.top-right').animate({transform: "rotate(0deg) skewY(-270deg)"},
          {duration: quarter, step: function(){element.children('.top-right').css({transform: "rotate(0deg) skewY(-180deg)"});}}
        );
        element.children('.bottom-right').delay(quarter+70).animate({transform: "rotate(90deg) skewY(-270deg)"},
          {duration: quarter, step: function(){element.children('.bottom-right').css({transform: "rotate(90deg) skewY(-180deg)"});}}
        );
        element.children('.bottom-left').delay(quarter*2+70).animate({transform: "rotate(180deg) skewY(-270deg)"},
          {duration: quarter, step: function(){element.children('.bottom-left').css({transform: "rotate(180deg) skewY(-180deg)"});}}
        );
        element.children('.top-left').delay(quarter*3+70).animate({transform: "rotate(270deg) skewY(-270deg)"},
          {duration: quarter, step: function(){element.children('.top-left').css({transform: "rotate(270deg) skewY(-180deg)"});}}
        );


        $timeout(function(){
          element.parent().slideUp().addClass('mask');
        },quarter*4+70);
      }
    }
  };
});


appCtrl.directive('elemReady', function( $parse ) {
  return {
    restrict: 'A',
    link: function( $scope, elem, attrs ) {
      elem.ready(function(){
        $scope.$apply(function(){
          var func = $parse(attrs.elemReady);
          func($scope);
        })
      })
    }
  }
});
