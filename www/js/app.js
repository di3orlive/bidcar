// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var appStarter = angular.module('DiBCaR', ['ionic', 'starter.controllers', 'ionic.rating']);

appStarter.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
});

appStarter.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.map', {
    url: '/map',
    views: {
      'menuContent': {
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      }
    }
  })

  .state('app.bidsBeing', {
    url: '/bidsBeing',
    views: {
      'menuContent': {
        templateUrl: 'templates/bidsBeing.html',
        controller: 'BidsBeingsCtrl'
      }
    }
  })

  .state('app.drivers', {
    url: '/drivers',
    views: {
      'menuContent': {
        templateUrl: 'templates/drivers.html',
        controller: 'DriversCtrl'
      }
    }
  })

  .state('app.driver', {
    url: '/drivers/:id',
    views: {
      'menuContent': {
      templateUrl: 'templates/driver.html',
      controller: 'DriverCtrl'
      }
    }
  })

  .state('app.notify', {
    url: '/notify',
    views: {
      'menuContent': {
        templateUrl: 'templates/notify.html'
      }
    }
  })

  .state('app.guardians', {
    url: '/guardians',
    views: {
      'menuContent': {
        templateUrl: 'templates/guardians.html'
      }
    }
  })

  .state('app.mapActive', {
    url: '/mapActive',
    views: {
      'menuContent': {
        templateUrl: 'templates/mapActive.html',
        controller: 'MapCtrl'
      }
    }
  })

  .state('app.arrived', {
    url: '/arrived',
    views: {
      'menuContent': {
        templateUrl: 'templates/arrived.html'
      }
    }
  })

  .state('app.rateDriver', {
    url: '/rateDriver',
    views: {
      'menuContent': {
        templateUrl: 'templates/rateDriver.html',
        controller: 'rateDriverCtrl'
      }
    }
  })

  .state('app.driverPage', {
    url: '/driverPage',
    views: {
      'menuContent': {
        templateUrl: 'templates/driverPage.html',
        controller: 'driverPageCtrl'
      }
    }
  })

  .state('app.rideNow', {
    url: '/rideNow',
    views: {
      'menuContent': {
        templateUrl: 'templates/rideNow.html',
        controller: 'rideNowCtrl'
      }
    }
  })

  .state('app.bitReq', {
    url: '/bitReq',
    views: {
      'menuContent': {
        templateUrl: 'templates/bitReq.html'
      }
    }
  })

  .state('app.bitBet', {
    url: '/bitBet',
    views: {
      'menuContent': {
        templateUrl: 'templates/bitBet.html',
        controller: 'bitBetCtrl'
      }
    }
  })

  .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/bitBet');
});





